from django import template

register = template.Library()


def hex_to_string(new_hex):
    if new_hex != None:
        return bytes.fromhex(new_hex).decode("utf-8")
    print(new_hex)
    return


register.filter(hex_to_string)
