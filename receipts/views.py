from attr import fields
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from receipts.models import Receipt, Account, ExpenseCategory
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import redirect
from django.views.generic.list import ListView


# Create your views here.


class ReceiptListView(ListView, LoginRequiredMixin):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Receipt.objects.filter(purchaser=self.request.user)


class AccountListView(ListView, LoginRequiredMixin):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryListView(ListView, LoginRequiredMixin):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return ExpenseCategory.objects.filter(owner=self.request.user)


class CreateAccountView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accounts/new.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        form.save_m2m()
        return redirect("home")


class CreateExpenseCategoryView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "categories/new.html"
    fields = ["name"]

    def form_valid(self, form):
        expenseCategory = form.save(commit=False)
        expenseCategory.owner = self.request.user
        expenseCategory.save()
        form.save_m2m()
        return redirect("home")


class CreateReceiptView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "account", "category"]
    success_url = reverse_lazy("receipts_list")

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        form.save_m2m()
        return redirect("home")
