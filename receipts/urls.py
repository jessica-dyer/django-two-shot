from django.contrib.auth import views as auth_views
from django.urls import include, path, reverse_lazy
from receipts.views import (
    CreateAccountView,
    CreateExpenseCategoryView,
    ReceiptListView,
    CreateReceiptView,
    AccountListView,
    ExpenseCategoryListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", CreateReceiptView.as_view(), name="receipt_new"),
    path("accounts/create/", CreateAccountView.as_view(), name="account_new"),
    path(
        "categories/create/",
        CreateExpenseCategoryView.as_view(),
        name="expense_category_new",
    ),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
]
